import { createSlice } from '@reduxjs/toolkit';

export const journalSlice = createSlice({
    name: 'journal',
    initialState: {
        isSaving: false,
        MessageSaved: '',
        notes: [],
        active: null,
    },
    reducers: {
        savingNewNote: ( state ) => {
            state.isSaving = true;
            state.MessageSaved ='';
        },
        addNewEmptyNote: (state, action ) => {
            state.notes.push( action.payload );
            state.isSaving = false;
        },
        setActiveNote: (state, action) => {
            state.active = action.payload;
            state.MessageSaved = '';
        },
        setNotes: (state, action ) => {
            state.notes = action.payload;
        },
        setSaving: (state, /* action */ ) => {
            state.isSaving = true;
            state.MessageSaved ='';
        },
        noteUpdated: (state, action ) => {
            state.isSaving= true;
            state.notes = state.notes.map (note =>{
                if( note.id === action.payload.id ){
                    console.log( state.active );
                    return  state.active;
                } 
                return note;
            });

            state.MessageSaved = `${ action.payload.title }, actualizada correctamente`;
            state.isSaving = false;
        },
        setPhotosToActiveNote: (state, action  ) => {
            state.active.imageUrls = [ ...state.active.imageUrls, ...action.payload ];
            state.isSaving = false;
        },
        clearNotesLogout: (state, action  ) => {
            state.isSaving = false;
            state.active = null;
            state.MessageSaved = '';
            state.notes = [];
        },
        deleteNoteById: (state, action ) => {
            state.active = null;
            state.notes = state.notes.filter (note => note.id !== action.payload );
        },
    }
});


export const { savingNewNote, addNewEmptyNote, setActiveNote, setNotes, setSaving, noteUpdated, setPhotosToActiveNote, clearNotesLogout, deleteNoteById } = journalSlice.actions;