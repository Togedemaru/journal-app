import { collection, deleteDoc, doc, setDoc } from "firebase/firestore/lite";
import { FirebaseDB } from "../../firebase/config";
import { addNewEmptyNote, deleteNoteById, noteUpdated, savingNewNote, setActiveNote, setNotes, setPhotosToActiveNote, setSaving } from "./journalSlice";
import { loadNotes } from '../../helpers'
import { fileUpload } from '../../helpers';
import { logoutFirebase } from "../../firebase/providers";
import { logout } from "../auth";


export const startNewNote = () => {
    return async ( dispatch, getState ) => {

        dispatch( savingNewNote() );
        

        const { uid } = getState().auth.user;
        console.log(uid);

        const newNote = {
            title: '',
            body: '',
            date: '',
            imageUrls: [],
        }

        const newDoc = doc( collection( FirebaseDB, `${ uid }/journal/notes` ) );
        const setDocResp = await setDoc( newDoc, newNote );

        newNote.id = newDoc.id;

        dispatch( addNewEmptyNote( newNote ) );
        dispatch( setActiveNote( newNote ) );
    };
}


export const startLoadingNotes = () => {
    return async ( dispatch, getState ) => {
        const { uid } = getState().auth.user;
        if( !uid  ) throw new Error('No existe el UID');
        const notes = await loadNotes( uid );

        dispatch( setNotes( notes ) );

    }
}

export const startSavingNote = () => {
    return async ( dispatch, getState ) => {
        dispatch( setSaving() );

        const { uid } = getState().auth.user;
        if( !uid  ) throw new Error('No existe el UID');
        const { active: note } = getState().journal;

        const noteToFirestore = { ...note };
        delete noteToFirestore.id;

        const docRef = doc( FirebaseDB, `${ uid }/journal/notes/${ note.id }` );
        const resp = await setDoc( docRef, noteToFirestore, { merge: true } );

        dispatch( noteUpdated( note ));

    }
}


export const starUploadingFiles = ( files = [] ) => {
    return async ( dispatch, getState ) => {
        dispatch( setSaving() );
        const fileUploadPromises = [];
        for (const file of files) {
            fileUploadPromises.push( fileUpload( file ) );
        }
        const photosURLs = await Promise.all(fileUploadPromises);
        console.log(photosURLs);
        dispatch( setPhotosToActiveNote( photosURLs ) );

    }
}

export const startDeletingNote = () => {
    return async ( dispatch, getState ) => {
        const { uid } = getState().auth.user;
        const { active: note } = getState().journal;
        const docRef = doc( FirebaseDB, `${ uid }/journal/notes/${ note.id }` );
        await deleteDoc( docRef );

        dispatch( deleteNoteById( note.id ) );
    }
}