import { createSlice } from '@reduxjs/toolkit';

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        status: 'checking',
        errorMessage: null,
        user:{ 
            uid: null,
            email: null,
            displayName: null,
            photoURL: null
        }

    },


    reducers: {
        login: (state, { payload } ) => {
            state.status = 'authenticated',
            state.errorMessage = null,
            state.user ={ 
                uid: payload.uid,
                email: payload.email,
                displayName: payload.displayName,
                photoURL: payload.photoURL
            }
        },


        logout: ( state, { payload } ) => {
            state.status = 'not-authenticated',
            state.errorMessage = payload,
            state.user ={ 
                uid: null,
                email: null,
                displayName: null,
                photoURL: null
            }
        },


        checkingCredentials: ( state ) => {
            state.status = 'checking';
        },
    }
});


export const { login, logout, checkingCredentials } = authSlice.actions;
