import { checkingCredentials, login, logout } from "./";
import { logoutFirebase, registerUserWithEmail, signInWithEmail, signInWithGoogle } from '../../firebase/providers'
import { clearNotesLogout } from "../journal/journalSlice";


export const checkingAuthentication = ( email, password ) => {
    return async ( dispatch ) => {
        dispatch( checkingCredentials() );
    };
}

export const startGoogleSignIn = (  ) => {
    return async ( dispatch ) => {
        dispatch( checkingCredentials() );
        const result = await signInWithGoogle();
        if(!result.okey) return dispatch ( logout( result.errorMessage ) );
        delete result.okey;
        dispatch ( login( result ) );
        
    };
}

export const startEmailSignIn = ( email, password ) => {
    return async ( dispatch ) => {
        dispatch( checkingCredentials() );
        const result = await signInWithEmail( email, password );
        if(!result.okey) return dispatch ( logout( result.errorMessage ) );
        delete result.okey;
        dispatch ( login( result ) );
        
    };
}


export const singUpwithEmail = ( displayName, email, password ) => {
    return async ( dispatch ) => {
        dispatch( checkingCredentials() );
        const result = await registerUserWithEmail( displayName, email, password );
        if(!result.okey) return dispatch ( logout( result.errorMessage  ) );
        delete result.okey;
        dispatch ( login( result ) );
        
    };
}

export const startLogout = () => {
    return async ( dispatch ) => {
        await logoutFirebase();
        dispatch( clearNotesLogout() );
        dispatch( logout() );

    }
}