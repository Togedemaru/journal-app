import { UploadOutlined } from "@mui/icons-material";
import SaveOutlined from "@mui/icons-material/SaveOutlined";
import { Button, Grid, IconButton, TextField, Typography } from "@mui/material";
import { useEffect, useMemo, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import 'sweetalert2/dist/sweetalert2.css';
import { useForm } from "../../hooks/useForm";
import { setActiveNote } from "../../store/journal/journalSlice";
import { startDeletingNote, startSavingNote, starUploadingFiles } from "../../store/journal/thunks";
import { ImageGallery } from "../components";

export const NoteView = () => {

    const { active, MessageSaved, isSaving } = useSelector( state => state.journal );

    const { body, title, date, onInputChange, formState } = useForm( active );
    
    const dispatch = useDispatch();

    const dateStr = useMemo( () => {
        const newDate = new Date();
        return newDate.toUTCString();
    }, [ date ]);

    const fileInputRef = useRef();


    useEffect(() => {
      dispatch( setActiveNote( formState ) );
    }, [formState]);

    
    useEffect(() => {
        if(MessageSaved.length > 0){
            Swal.fire( 'Nota actualizada', MessageSaved, 'success' );
        }
    }, [MessageSaved]);

    const onSaveNote = () => {
        dispatch( startSavingNote() );
    }

    const onFileInputChange = ( { target } ) => {
        if( target.files === 0 ) return;

        dispatch( starUploadingFiles(target.files) );
    }

    const onDelete = () => {
        dispatch( startDeletingNote() );
    }
    


    return (
        <Grid container direction="row" justifyContent="space-between" alignItems="center" sx={{mb: 1 }}>
            <Grid item>
                <Typography fontSize={ 39 } fontWeight="ligth"> { dateStr } </Typography>
            </Grid>

            <Grid item>
                <input 
                    type="file"
                    multiple
                    ref={ fileInputRef }
                    onChange={ onFileInputChange }
                    style={{ display: 'none' }}
                />
                <IconButton disabled={ isSaving } color="primary" onClick={ ()=> fileInputRef.current.click() }>
                    <UploadOutlined />
                </IconButton>
            </Grid>

            <Grid item>
                <Button onClick={ onSaveNote } disabled={ isSaving } color="primary" sx={{ padding: 2 }}>
                    <SaveOutlined sx={{ fontSize: 30, mr: 1 }} />
                    Guardar
                </Button>
            </Grid>

            <Grid container>
                <TextField 
                    type="text"
                    variant="filled" 
                    fullWidth
                    placeholder="Ingrese un título"
                    label="Título"
                    sx={{ border: "none", mb: 1 }}
                    onChange={ onInputChange }
                    name="title"
                    value={ title }
                />
            </Grid>

            <Grid container>
                <TextField
                    type="text"
                    variant="filled"
                    fullWidth
                    multiline
                    placeholder="¿Que sucedió en el día de hoy?"
                    minRows={ 5 }
                    sx={{ border: "none", mb: 1 }}
                    onChange={ onInputChange }
                    name="body"
                    value={ body }
                />
            </Grid>

            <Grid container
                justifyContent="end"
            >
                <Button
                    onClick={ onDelete }
                    sx={{ mt: 2 }}
                    color='error'
                >
                    Borrar
                </Button>
            </Grid>



            <ImageGallery images = { active.imageUrls } />


        </Grid>
    );
}
