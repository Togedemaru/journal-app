import { Link as RouterLink } from 'react-router-dom';
import { Google } from "@mui/icons-material";
import { Alert, Button, Grid, Link, TextField, Typography } from "@mui/material";
import { AuthLayout } from '../layout/AuthLayout';
import { useForm } from '../../hooks';
import { useDispatch, useSelector } from 'react-redux';
import { startEmailSignIn, startGoogleSignIn } from '../../store/auth';
import { useMemo, useState } from 'react';


const formData = {
    email: '',
    password: '',
}

const formValidations = {
    email: [  (value) => value.includes('@')  , 'El correo debe de tener un @'],
    password: [ (value) => value.length >= 6 ,  'El password debe de tener más de 6 letras' ],
}

export const LoginPage = () => {

    const dispatch = useDispatch();

    const { status, errorMessage } = useSelector( state => state.auth );

    const { email, password, onInputChange, emailValid, passwordValid, isFormValid } = useForm(formData, formValidations);

    const [formSubmited, setFormSubmited] = useState(false);

    const onSubmit = ( event ) => {
        event.preventDefault();
        setFormSubmited(true);
        if( !isFormValid ) return false;
        dispatch( startEmailSignIn( email, password ) );
    }

    const onGoogleSignIn = () => {
        console.log('googleSignIn');
        dispatch( startGoogleSignIn(  ) );
    };



    const isAuthenticading = useMemo( ()=> status === 'checking', [status] );

    return (
        <AuthLayout title='Login!'>
            
            <form onSubmit={ onSubmit }>
                <Grid container >
                    <Grid item xs={ 12 } sx={{ mt: 2 }}>
                        <TextField 
                            label="correo" 
                            type="email" 
                            placeholder="correo@google.com" 
                            fullWidth
                            name="email"
                            value={email}
                            onChange={ onInputChange }
                            error= { !!emailValid && formSubmited }
                            helperText={ emailValid }
                            autoComplete="username"
                        />
                    </Grid>

                    <Grid item xs={ 12 } sx={{ mt: 2 }}>
                        <TextField 
                            label="contrseña" 
                            type="password" 
                            placeholder="contraseña" 
                            fullWidth
                            name="password"
                            value={ password }
                            onChange={ onInputChange }
                            error= { !!passwordValid && formSubmited }
                            helperText={ passwordValid }
                            autoComplete="current-password"
                        />
                    </Grid>

                    <Grid container spacing={ 2 } sx={{ mb: 2, mt: 1 }}>
                        <Grid item xs={ 12 } display={ !!errorMessage ? '' : 'none' }> 
                            <Alert severity="error">{ errorMessage }</Alert>
                        </Grid>
                    </Grid>

                    <Grid container spacing={ 2 } sx={{ mb: 2, mt: 1 }}>
                        <Grid item xs={ 12 } sm={ 6 }> 
                            <Button disabled = { isAuthenticading } variant="contained" fullWidth type="submit">
                                Login
                            </Button>
                        </Grid>

                        <Grid item xs={ 12 } sm={ 6 }> 
                            <Button  disabled = { isAuthenticading } variant="contained" fullWidth onClick={ onGoogleSignIn }>
                                <Google />
                                <Typography sx={{ ml: 1 }} > Google </Typography>
                            </Button>
                        </Grid>
                    </Grid>

                    <Grid container direction="row" justifyContent="end">
                        <Link component={ RouterLink } color="inherit" to="/auth/register">
                            Crear una cuenta
                        </Link>
                    </Grid>

                </Grid>

            </form>

        </AuthLayout>
    );
}
