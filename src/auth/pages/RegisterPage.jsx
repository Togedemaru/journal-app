import { Link as RouterLink } from 'react-router-dom';
import { Alert, Button, Grid, Link, TextField, Typography } from "@mui/material";
import { AuthLayout } from '../layout/AuthLayout';
import { useForm } from '../../hooks';
import { useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { singUpwithEmail } from '../../store/auth';

export const RegisterPage = () => {

    const dispatch = useDispatch();

    const formData = {
        displayName: '',
        email: '',
        password: '',
    }

    const formValidations = {
        email: [  (value) => value.includes('@')  , 'El correo debe de tener un @'],
        password: [ (value) => value.length >= 6 ,  'El password debe de tener más de 6 letras' ],
        displayName: [ (value) => value.length >= 1 ,  'El nombre es obligatorio' ],
    }

    const { formstate, displayName, email, password, onInputChange, 
        isFormValid, displayNameValid, emailValid, passwordValid 
    } = useForm( formData, formValidations );


    const [formSubmited, setFormSubmited] = useState(false);

    
    const onSubmit = ( event ) => {
        event.preventDefault();
        setFormSubmited(true);
        if( !isFormValid ) return;

        //Register user
        dispatch( singUpwithEmail( displayName, email, password ) );
        
    }

    const { status, errorMessage } = useSelector( state => state.auth );
    const isCheckingAuthentication = useMemo( () => status === 'checking' );

    return (
        <AuthLayout title='Create Account!'>
            <form onSubmit={ onSubmit }>
                <Grid container>
                    <Grid item xs={ 12 } sx={{ mt: 2 }}>
                        <TextField 
                            label="Nombre Completo" 
                            type="text" 
                            placeholder="Luis Ballesteros" 
                            fullWidth
                            name="displayName"
                            value={ displayName }
                            onChange={ onInputChange }
                            error= { !!displayNameValid && formSubmited }
                            helperText={ displayNameValid }
                        />
                    </Grid>

                    <Grid item xs={ 12 } sx={{ mt: 2 }}>
                        <TextField 
                            label="correo" 
                            type="email" 
                            placeholder="correo@google.com" 
                            fullWidth
                            name="email"
                            value={ email }
                            onChange={ onInputChange }
                            error= { !!emailValid && formSubmited }
                            helperText={ emailValid }
                            autoComplete="username"
                        />
                    </Grid>

                    <Grid item xs={ 12 } sx={{ mt: 2 }}>
                        <TextField 
                            label="contrseña" 
                            type="password" 
                            placeholder="contraseña" 
                            fullWidth
                            name="password"
                            value={ password }
                            onChange={ onInputChange }
                            error= { !!passwordValid && formSubmited }
                            helperText={ passwordValid }
                            autoComplete="current-password"
                        />
                    </Grid>

                    <Grid container spacing={ 2 } sx={{ mb: 2, mt: 1 }}>
                        <Grid item xs={ 12 } display={ !!errorMessage ? '' : 'none' }> 
                            <Alert severity="error">{ errorMessage }</Alert>
                        </Grid>
                    </Grid>

                    <Grid container spacing={ 2 } sx={{ mb: 2, mt: 1 }}>
                        <Grid item xs={ 12 }> 
                            <Button disabled={ isCheckingAuthentication } type="submit" variant="contained" fullWidth>
                                Register Account
                            </Button>
                        </Grid>
                    </Grid>

                    <Grid container direction="row" justifyContent="end">
                        <Typography sx={{ mr: 1 }} > ¿Ya tienes cuenta?  </Typography>
                        <Link component={ RouterLink } color="inherit" to="/auth/login">
                            Ingresar
                        </Link>
                    </Grid>

                </Grid>

            </form>

        </AuthLayout>
    );
}