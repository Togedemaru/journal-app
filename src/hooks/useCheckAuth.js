import { onAuthStateChanged } from 'firebase/auth';
import { FireBaseAuth } from '../firebase/config';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { logout, login } from '../store/auth';
import { startLoadingNotes } from '../store/journal';


export const useCheckAuth = () => {
    
    const { status } = useSelector(state => state.auth);

    const dispatch = useDispatch();

    useEffect(() => {

        onAuthStateChanged( FireBaseAuth, async ( user ) => {
            if( !user ) return dispatch( logout() );
            const { uid, displayName, email, photoURL } = user;
            dispatch( login({ uid, displayName, email, photoURL }) );
            dispatch( startLoadingNotes() )
        } );

    }, [])

    return status;
}
