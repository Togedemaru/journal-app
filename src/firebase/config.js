import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth'
import { getFirestore } from 'firebase/firestore/lite';

const firebaseConfig = {
    apiKey: "AIzaSyAFa-wLfIoQEtrmq-GDYIFsXZL0tEuHVDI",
    authDomain: "journalapp-aacbb.firebaseapp.com",
    projectId: "journalapp-aacbb",
    storageBucket: "journalapp-aacbb.appspot.com",
    messagingSenderId: "326704601962",
    appId: "1:326704601962:web:ed7873fe238170b321e313"
};

export const FirebaseApp = initializeApp(firebaseConfig);

export const FireBaseAuth = getAuth( FirebaseApp );

export const FirebaseDB = getFirestore( FirebaseApp );