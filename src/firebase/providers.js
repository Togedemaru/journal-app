import { createUserWithEmailAndPassword, GoogleAuthProvider, signInWithEmailAndPassword, signInWithPopup, updateProfile } from 'firebase/auth';
import { FireBaseAuth } from "./config";

const googleProvider = new GoogleAuthProvider();

export const signInWithGoogle = async () => {
    try{
        const result = await signInWithPopup( FireBaseAuth, googleProvider );
        const credential = GoogleAuthProvider.credentialFromResult( result );
        const { displayName, email, photoURL, uid } = result.user;
        return{
            okey: true,
            displayName, 
            email, 
            photoURL, 
            uid
        }
    }catch( error ){
        const errorCode = error.code;
        const errorMessage = error.message;
        return{
            okey: false,
            errorMessage
        }
    }
}


export const signInWithEmail = async ( email, password ) => {
    console.log(email);
    try{
        const result = await signInWithEmailAndPassword( FireBaseAuth, email, password );
        const { displayName, photoURL, uid } = result.user;
        return{
            okey: true,
            displayName, 
            email, 
            photoURL, 
            uid
        }
    }catch( error ){
        const errorCode = error.code;
        const errorMessage = error.message;
        return{
            okey: false,
            errorMessage
        }
    }
}



export const registerUserWithEmail = async ( displayName, email, password ) => {
    try{
        const result = await createUserWithEmailAndPassword( FireBaseAuth, email, password );
        const { uid, photoURL } = result.user;
        await updateProfile( FireBaseAuth.currentUser, { displayName }  );
        return{
            okey: true,
            email,
            displayName,
            photoURL, 
            uid
        }
    }catch( error ){
        const errorCode = error.code;
        const errorMessage = error.message;
        return{
            okey: false,
            errorMessage
        }
    }
}



export const logoutFirebase = async () => {
    return await FireBaseAuth.signOut();
}